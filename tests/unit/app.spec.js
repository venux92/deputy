jest.mock('Firebase');

import { shallowMount } from '@vue/test-utils';
import Firebase from 'firebase';
import App from '@/App.vue';
import * as Roles from '@/models/Roles';
import * as Users from '@/models/Users';

describe('App.vue', () => {
  const wrapper = shallowMount(App);
  let resetUsersDB = true;
  let resetRolesDB = true;

  beforeAll(async () => {
    await Firebase.firestore().enableNetwork();
  });
  // close firebase connection on exit
  afterAll(async () => {
    await Firebase.firestore().disableNetwork();
  });
  // check that the data provided for the test is correctly set
  it('the default users are filled with the test values', () => {
    expect(wrapper.vm.usersDefault.length).toBe(5);
  });
  // check that the data provided for the test is correctly set
  it('the default roles are filled with the test values', () => {
    expect(wrapper.vm.rolesDefault.length).toBe(5);
  });
  // read if any users are already set and store the users in the array if any present
  it('read users from firestore + add previously set users to the users array if any', async (done) => {
    await wrapper.vm.getUsers();
    expect(wrapper.vm.users.length).toBeGreaterThanOrEqual(0);
    done();
  });
  // read if any roles are already set and store the roles in the array if any present
  it('read roles from firestore + add previously set roles to the roles array if any', async (done) => {
    await wrapper.vm.getRoles();
    expect(wrapper.vm.roles.length).toBeGreaterThanOrEqual(0);
    done();
  });
  // add users to firestore if any are present
  it('add default users to firestore if necessary', async () => {
    // we just want to check if the users array is empty otherwise it means
    // that it has already been imported so we don't want to import it again
    if (wrapper.vm.users.length === 0) {
      await wrapper.vm.setUsers();
      expect(wrapper.vm.users.length).toBe(5);
    } else {
      resetUsersDB = false;
      return true;
    }
  });
  // add roles into firestore if any are present
  it('add default roles to firestore if necessary', async () => {
    // we just want to check if the roles array is empty otherwise it means
    // that it has already been imported so we don't want to import it again
    if (wrapper.vm.roles.length === 0) {
      await wrapper.vm.setRoles();
      expect(wrapper.vm.roles.length).toBe(5);
    } else {
      resetRolesDB = false;
      return true;
    }
  });
  // check if users are correctly set into firestore
  it('users are correctly set to firestore', async () => {
    await wrapper.vm.totalUsers();
    expect(wrapper.vm.total.users).toBe(5);
  });
  // check if roles are correctly set into firestore
  it('roles are correctly set to firestore', async () => {
    await wrapper.vm.totalRoles();
    expect(wrapper.vm.total.roles).toBe(5);
  });
  // run a test to make sure we are getting the correct answer
  it('get subordinates by user id', async () => {
    await wrapper.vm.getSubOrdinates(1);
    // based on the example provided
    // getSubOrdinates(1); // should return [{"Id": 2,"Name": "Emily
    // Employee","Role": 4}, {"Id": 3,"Name": "Sam Supervisor","Role": 3},
    // {"Id": 4,"Name": "Mary Manager","Role": 2}, {"Id": 5, "Name": "Steve
    //   Trainer","Role": 5}]
    expect(wrapper.vm.result.length).toBe(4);
  });
  // remove from firestore any users added
  it('remove any users added to firestore', async (done) => {
    // can we remove the data from the DB?
    if (resetUsersDB) {
      await Users.deleteCollection().then(async () => {
        await wrapper.vm.totalUsers();
        expect(wrapper.vm.total.users).toBe(0);
        done();
      });
    } else {
      done();
      return true;
    }
  });
  // remove from firestore any roles added
  it('remove any roles added to firestore', async (done) => {
    // can we remove the data from the DB?
    if (resetRolesDB) {
      await Roles.deleteCollection().then(async () => {
        await wrapper.vm.totalRoles();
        expect(wrapper.vm.total.roles).toBe(0);
        done();
      });
    } else {
      done();
      return true;
    }
  });
});
