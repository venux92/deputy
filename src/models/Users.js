import * as FireStore from './FireStore';

// get all users and return an array of users
export function getUsers() {
  const array = [];
  return FireStore.getCollection('users').then((snapshot) => {
    snapshot.forEach((user) => {
      array.push(user.data());
    });
    return array;
  });
}
// add all users to the collection and return an array of users
export async function setUsers(roles) {
  const array = [];
  await Promise.all(roles.map(async role => FireStore.addToCollection('users', role).then(() => {
    array.push(role);
  })));
  return array;
}
// total of users in firestore
export function totalItems() {
  return FireStore.totalInCollection('users').then(total => total);
}
// delete all users
export function deleteCollection() {
  return FireStore.deleteCollection('users');
}
