import db from '../main';

// fetch collection from firestore
export function getCollection(name) {
  return db.collection(name).orderBy('Id').get().then(snapshot => snapshot);
}
// add item to a collection
export function addToCollection(name, data) {
  // add object to the corresponding array
  return db.collection(name).add(data).then(() => true);
}
// total of items in a colelction
export function totalInCollection(name) {
  return db.collection(name).get().then(snapshot => snapshot.size);
}
// delete collection from firestore
export function deleteCollection(name) {
  return db.collection(name).get().then(async (snapshot) => {
    // get all document in the collection
    const documents = snapshot.docs;
    await Promise.all(documents.map(async (doc) => {
      // delete the document
      await db.collection(name).doc(doc.id).delete();
    }));
  });
}
