import * as FireStore from './FireStore';

// get all roles and return an array of roles
export function getRoles() {
  const array = [];
  return FireStore.getCollection('roles').then((snapshot) => {
    snapshot.forEach((role) => {
      array.push(role.data());
    });
    return array;
  });
}
// add all roles to the collection and return an array of roles
export async function setRoles(roles) {
  const array = [];
  await Promise.all(roles.map(async role => FireStore.addToCollection('roles', role).then(() => {
    array.push(role);
  })));
  return array;
}
// total of roles in firestore
export function totalItems() {
  return FireStore.totalInCollection('roles').then(total => total);
}
// delete all roles
export function deleteCollection() {
  return FireStore.deleteCollection('roles');
}
