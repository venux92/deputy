import Vue from 'vue';
import Firebase from 'firebase';
import App from './App.vue';

Vue.config.productionTip = false;
Vue.component('button-custom', require('./components/ButtomCustom').default);
Vue.component('table-list', require('./components/TableList').default);

// firebase configuration
const config = {
  apiKey: process.env.VUE_APP_FIREBASE_APIKEY,
  authDomain: process.env.VUE_APP_FIREBASE_AUTHDOMAIN,
  databaseURL: process.env.VUE_APP_FIREBASE_DATABASEURL,
  projectId: process.env.VUE_APP_FIREBASE_PROJECTID,
  messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGINGSENDERID,
  appId: process.env.VUE_APP_FIREBASE_APPID,
};

const firebase = Firebase.initializeApp(config);
// get firestore instance
const db = firebase.firestore();
export default db;

new Vue({
  render: h => h(App),
}).$mount('#app');
