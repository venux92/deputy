## Project setup
This project use Vue.js + Firestore to store **roles** and **users** data.\
The main purpose is to display all subordinates related to a user/role.

### Clone repository and install dependencies
```$xslt
git clone https://gitlab.com/venux92/deputy.git
```
```
yarn install
```
> if you are using npm replace yarn by npm

### Copy Environement variables
copy `.env.local` to the root of your project contained in the zip file

### Run locally
```
yarn run serve
```

### Access Program
```
http://localhost:8080/
```

### Run your unit tests
```
yarn run test:unit
```
> If all test pass you should see the same result as below

![test](https://i.ibb.co/RzQMwqZ/Screen-Shot-2019-07-07-at-5-19-38-pm.png)
